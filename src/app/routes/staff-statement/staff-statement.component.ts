import {Component, OnInit} from '@angular/core';
import {SalaryItem} from './models/salary-item';
import {Detail, DetailSummary} from './models/detail';
import {Parameter} from './models/parameter';
import {PersonalAccountStatementService} from './services/personal-account-statement.service';
import {DepositedSalaryService} from './services/deposited-salary.service';
import {PersonalAccountIncomeService} from './services/personal-account-income.service';
import {PersonalAccountDiscountService} from './services/personal-account-discount.service';
import {AdvenceAidService} from './services/advence-aid.service';

@Component({
  selector: 'upn-staff-statement',
  templateUrl: './staff-statement.component.html',
  styles: [],
})
export class StaffStatementComponent implements OnInit {

  public salaryItems: SalaryItem[] = [];
  public detail: Detail;
  private params: Parameter = new Parameter();
  public searching = false;
  public detailSearching = false;

  constructor(private personalAccountStatementService: PersonalAccountStatementService,
              private depositedSalaryService: DepositedSalaryService,
              private personalAccountIncomeService: PersonalAccountIncomeService,
              private personalAccountDiscountService: PersonalAccountDiscountService,
              private advenceAidService: AdvenceAidService) {
  }

  ngOnInit() {
  }

  search(params) {
    this.salaryItems = [];
    this.detail = null;
    this.searching = true;

    this.params = {
      data: {
        entity: params.entity,
        month: params.month,
        year: params.year,
        cta_cte: params.employee.doc_number,
        id_persona: params.employee.id,
      },
    };


    this.personalAccountStatementService.getStateAccount(this.params)
      .subscribe(r => {
        this.parseSalaryItems(r.data.item);
        this.searching = false;
      });
  }

  private parseSalaryItems(data: any) {
    if (data.sueldo_depositado > 0) {
      this.salaryItems.push(
        new SalaryItem(1, 'SUELDO DEPOSITADO',
          data.sueldo_depositado, 'assets/icons/sta-sueldodepositado.svg'));

      if (data.adelanto_ayudas_c > 0) {
        this.salaryItems.push(
          new SalaryItem(2, 'ADELANTO DE AYUDAS',
            data.adelanto_ayudas, 'assets/icons/sta-adelantoayudas.svg'));
      }
      if (data.adelanto_gratificaciones_c > 0) {
        this.salaryItems.push(
          new SalaryItem(3, 'ADELANTO DE GRATIFICACIONES',
            data.adelanto_gratificaciones, 'assets/icons/sta-adelantogratificaciones.svg'));
      }
      if (data.a_rendir_c > 0) {
        this.salaryItems.push(
          new SalaryItem(4, 'A RENDIR', data.a_rendir, 'assets/icons/sta-arendir.svg'));
      }
    }
  }

  searchDetail(id: number) {
    this.detailSearching = true;

    this.detail = new Detail();
    this.detail.id = id;
    this.detail.items = [];
    this.params.data.id_cuentaaasi = null;
    this.params.data.id_cta_cte = this.params.data.cta_cte;

    switch (id) {
      case 1: {
        this.detail.title = 'Sueldo depositado';
        const detailSummary = new DetailSummary();
        this.depositedSalaryService.getListDepositedSalary(this.params).subscribe(
          data_response => {
            detailSummary.descuentos = data_response.data.item.l_descuentos_total;
            detailSummary.ingresos = data_response.data.item.l_ingresos;
            detailSummary.depositado = data_response.data.item.l_sueldo_depositado;
            this.detail.total = data_response.data.item.l_sueldo_depositado;

            this.personalAccountIncomeService.getPersonalAccountIcome(this.params)
              .subscribe(
                r => {
                  detailSummary.itemsIngresos = r.data.items;
                }, error => {
                });

            this.personalAccountDiscountService.getListIccomeDiscount(this.params).subscribe(
              r => {
                detailSummary.itemsDescuentos = r.data.items;
              }, error => {
              });

          },
          error => {
          });

        this.detail.summary = detailSummary;
        this.detailSearching = false;


        break;
      }
      case 2: {
        this.detail.title = 'Adelanto de ayudas';
        this.params.data.id_cuentaaasi = '1135060';
        this.advenceAidService.getListAdvanceAid(this.params)
          .subscribe(
            r => {
              this.detail.items = r.data.items;
              this.detail.total = r.data.total.total;
              this.detailSearching = false;
            }, error => {
            });
        break;
      }
      case 3: {
        this.detail.title = 'Adelanto de gratificaciones';
        this.params.data.id_cuentaaasi = '1135016';
        this.advenceAidService.getListAdvanceAid(this.params)
          .subscribe(
            r => {
              this.detail.items = r.data.items;
              this.detail.total = r.data.total.total;
              this.detailSearching = false;
            }, error => {
            });

        break;
      }
      case 4: {
        this.detail.title = 'A rendir';
        this.params.data.id_cuentaaasi = '1135007';
        this.advenceAidService.getListAdvanceAid(this.params)
          .subscribe(
            r => {
              this.detail.items = r.data.items;
              this.detail.total = r.data.total.total;
              this.detailSearching = false;
            }, error => {
            });
        break;
      }
      case 0: {
        const a = document.createElement('a');
        const fileName: string = this.params.data.cta_cte + ' ' + 'boleta.pdf';
        this.personalAccountStatementService.downloadPdf(this.params).subscribe(r => {
            const blob = new Blob([r], {type: 'application/pdf'});
            const url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = fileName;
            a.click();
            this.detailSearching = false;
          },
          error => {
            this.detailSearching = false;
          });
        break;
      }
      default: {
        break;
      }
    }
  }

}
