import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class DepositedSalaryService {

  private url = environment.urlAPI + 'mobile/st_data_items';

  constructor(private http: HttpClient) {
  }

  getListDepositedSalary(sendData: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.url, sendData);
  }


}
