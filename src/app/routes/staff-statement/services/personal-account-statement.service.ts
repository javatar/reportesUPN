import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class PersonalAccountStatementService {
  private url = environment.urlAPI + 'mobile/st_data';
  // private urlDownloadPdf = 'report/payment_ticket';
  private urlDownloadPdf = 'humantalent/payments-tickets';

  constructor(private http: HttpClient) {
  }

  getStateAccount(sendData: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.url, sendData);
  }

  downloadPdf(sendData: any): Observable<Blob> {
    return this.http.post(this.urlDownloadPdf, sendData, {responseType: 'blob'});
  }

  // data:application/pdf;base64,
  DownloadPaymentPdf(send_data: any): any {
    const headers = new HttpHeaders();
    headers.set('Accept', 'application/pdf');
    return this.http.post(this.urlDownloadPdf, send_data, {headers: headers, responseType: 'blob'});
  }
}
