import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class PersonalAccountDiscountService {

  private url = environment.urlAPI + 'mobile/account_statement_salary_discounts_detail';

  constructor(private http: HttpClient) {
  }

  getListIccomeDiscount(sendData: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.url, sendData);
  }

}
