export class Parameter {
  data: ParameterData;
}

export class ParameterData {
  entity: string;
  month: string;
  year: string;
  cta_cte?: string;
  id_cta_cte ?: string;
  id_persona?: string;
  id_cuentaaasi ?: string;
}
