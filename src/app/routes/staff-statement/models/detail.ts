export class Detail {
  id: number;
  title: string;
  items: DetailItem[] = [];
  total: number;
  summary: DetailSummary;
}

export class DetailSummary {
  ingresos: number;
  descuentos: number;
  depositado: number;
  itemsIngresos: DetailItem[];
  itemsDescuentos: DetailItem[];
}

export class DetailItem {
  id?: string;
  comentario: string;
  valor: number;
  fecha: string;
  load?: boolean;
  open?: boolean;
  children ?: DetailItem[];
}
