export class SalaryItem {
  id: number;
  name: string;
  value: any;
  icon: string;

  constructor(id: number, name: string, value: any, icon: string) {
    this.id = id;
    this.name = name;
    this.icon = icon;
    this.value = value;
  }

}
