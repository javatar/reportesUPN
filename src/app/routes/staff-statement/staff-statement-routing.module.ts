import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {StaffStatementComponent} from './staff-statement.component';

const routes: Routes = [
  {
    path: '',
    component: StaffStatementComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StaffStatementRoutingModule {
}
