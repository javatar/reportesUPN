import {Component, Input, OnInit} from '@angular/core';
import {Detail} from '../../models/detail';

@Component({
  selector: 'upn-detail-statement',
  templateUrl: './detail-statement.component.html',
  styles: [],
})
export class DetailStatementComponent implements OnInit {
  @Input() detail: Detail;
  @Input() isLoading = false;

  constructor() {
  }

  ngOnInit() {
  }

}
