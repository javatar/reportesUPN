import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SalaryItem} from '../../models/salary-item';

@Component({
  selector: 'upn-summary-statement',
  templateUrl: './summary-statement.component.html',
  styles: [`
    button {
      border-radius: 0 !important;
      cursor: pointer;
      line-height: 1.5rem;
    }

    button > svg-icon {
      margin-left: -5px;
      margin-right: 10px;
      width: 30px;
    }

    span {
      font-weight: bold;
    }
  `],
})
export class SummaryStatementComponent implements OnInit {
  @Input() items: SalaryItem[];
  @Output() onSelect = new EventEmitter<number>();
  @Input() isLoading: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  selectItem(id: number) {
    this.onSelect.emit(id);
  }
}
