import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StaffStatementComponent} from './staff-statement.component';
import {StaffStatementRoutingModule} from './staff-statement-routing.module';
import {SummaryStatementComponent} from './components/summary-statement/summary-statement.component';
import {DetailStatementComponent} from './components/detail-statement/detail-statement.component';
import {FilterModule} from '../../tools/components/filter/filter.module';
import {PersonalAccountDiscountService} from './services/personal-account-discount.service';
import {PersonalAccountIncomeService} from './services/personal-account-income.service';
import {PersonalAccountStatementService} from './services/personal-account-statement.service';
import {DepositedSalaryService} from './services/deposited-salary.service';
import {AdvenceAidService} from './services/advence-aid.service';
import {EmployeeService, EntityService, MonthService, YearService} from '@providers/services';
import {LoaderModule} from '../../tools/components/loader/loader.module';

@NgModule({
  imports: [
    CommonModule,
    StaffStatementRoutingModule,
    FilterModule,
    LoaderModule
  ],
  declarations: [StaffStatementComponent, SummaryStatementComponent, DetailStatementComponent],
  providers: [
    PersonalAccountDiscountService,
    PersonalAccountIncomeService,
    PersonalAccountStatementService,
    DepositedSalaryService,
    AdvenceAidService,
    EntityService,
    YearService,
    MonthService,
    EmployeeService
  ]
})
export class StaffStatementModule {
}
