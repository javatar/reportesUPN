import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {Observable} from 'rxjs';
import {IResponse} from '@interfaces/response';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class TravelsService {

  private url = environment.urlAPI + 'mobile/tra_data';
  private urlDetail = environment.urlAPI + 'mobile/tra_data_details';


  constructor(private http: HttpClient) {
  }

  find(sendData: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.url, sendData);
  }

  findDetails(sendData: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.urlDetail, sendData);
  }
}
