import {Component, Input, OnInit} from '@angular/core';
import {Detail} from '../../models/detail';

@Component({
  selector: 'upn-detail-travel',
  templateUrl: './detail-travel.component.html'
})
export class DetailTravelComponent implements OnInit {
  @Input() data: Detail;
  @Input() isLoading: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
