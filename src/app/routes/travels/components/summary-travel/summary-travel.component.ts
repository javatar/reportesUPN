import {Component, Input, OnInit} from '@angular/core';
import {Summary} from '../../models/summary';

@Component({
  selector: 'upn-summary-travel',
  templateUrl: './summary-travel.component.html'
})
export class SummaryTravelComponent implements OnInit {
  @Input() isLoading = false;

  public chartLabels: string[] = ['Realizado', 'Saldo'];
  public chartData: number[] = [];
  public chartColors: any[] = [{backgroundColor: ['43a047', 'fb8c00']}];

  private _data: Summary;

  constructor() {
  }

  ngOnInit() {
  }

  @Input()
  set data(data: Summary) {
    this._data = data;
    if (data) {
      this.chartData = [data.realizado, data.saldo];
    }
  }

  get data() {
    return this._data;
  }

}
