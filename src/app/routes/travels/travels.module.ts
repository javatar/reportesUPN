import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TravelsRoutingModule} from './travels-routing.module';
import {TravelsComponent} from './travels.component';
import {FilterModule} from '../../tools/components/filter/filter.module';
import {EmployeeService, EntityService, MonthService, YearService} from '@providers/services';
import {TravelsService} from './services/travels.service';
import { DetailTravelComponent } from './components/detail-travel/detail-travel.component';
import { SummaryTravelComponent } from './components/summary-travel/summary-travel.component';
import {LoaderModule} from '../../tools/components/loader/loader.module';
import {ChartsModule} from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    TravelsRoutingModule,
    FilterModule,
    LoaderModule,
    ChartsModule
  ],
  declarations: [TravelsComponent, DetailTravelComponent, SummaryTravelComponent],
  providers: [
    EntityService,
    YearService,
    MonthService,
    EmployeeService,
    TravelsService
  ]
})
export class TravelsModule {
}
