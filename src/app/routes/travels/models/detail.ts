export class Detail {
  total: number;
  items?: DetailItem[];
}

export class DetailItem {
  id?: string;
  comentario: string;
  valor: number;
  fecha: string;
  load?: boolean;
  open?: boolean;
  children ?: DetailItem[];
}
