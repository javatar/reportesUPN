export class Summary {
  ppto: number;
  saldoInicial: number;
  totalPpto: number;
  realizado: number;
  saldo: number;
  porcentajeRealizado ?: number;
  childOpen = false;

  constructor(ppto: number, saldoInicial: number, totalPpto: number, realizado: number, saldo: number) {
    this.ppto = ppto;
    this.saldoInicial = saldoInicial;
    this.totalPpto = totalPpto;
    this.realizado = realizado;
    this.saldo = saldo;

    if (this.realizado > 0) {
      this.porcentajeRealizado = (this.realizado / this.totalPpto);
    }

  }

}
