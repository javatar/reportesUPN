import {Component, OnInit} from '@angular/core';
import {Parameter} from '../staff-statement/models/parameter';
import {TravelsService} from './services/travels.service';
import {Summary} from './models/summary';
import {Detail, DetailItem} from './models/detail';

@Component({
  selector: 'upn-travels',
  templateUrl: './travels.component.html'
})
export class TravelsComponent implements OnInit {
  private params: Parameter = new Parameter();
  public searchingSummary = false;
  public searchingDetail = false;
  public summary: Summary;
  public detail: Detail;


  constructor(private travelService: TravelsService) {
  }

  ngOnInit() {
  }

  search(params) {
    this.searchingSummary = true;
    this.searchingDetail = true;

    this.params = {
      data: {
        entity: params.entity,
        month: null,
        year: params.year,
        cta_cte: params.employee.doc_number
      }
    };

    this.travelService.find(this.params)
      .subscribe(r => {
        const _summary = r.data.travel;
        this.summary = new Summary(_summary.ppto, _summary.saldo_inicial, _summary.total_ppto, _summary.realizado, _summary.saldo);
      }, null, () => {
        this.searchingSummary = false;
      });

    this.travelService.findDetails(this.params)
      .subscribe(r => {
        this.detail = new Detail();
        this.detail.total = r.data.total.total;
        this.detail.items = [];

        for (const i of r.data.items) {
          const item = new DetailItem();
          item.comentario = i.comentario;
          item.fecha = i.fecha;
          item.valor = i.valor;

          this.detail.items.push(item);
        }
      }, null, () => {
        this.searchingDetail = false;
      });

  }

}
