import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from './login.service';
import {Router} from '@angular/router';
import {AuthService} from '@providers/services/auth.service';

@Component({
  selector: 'upn-login',
  templateUrl: './login.html'
})
export class LoginComponent implements OnInit {
  public msgError: string;

  public form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private authService: AuthService) {
  }

  ngOnInit() {
    if (this.authService.isLoggednIn()) {
      this.router.navigate(['/']);
    } else {
      this.form = this.formBuilder.group({
          email: ['', Validators.required],
          password: ['', Validators.required]
        }
      );
    }
  }

  public onSubmit(): void {
    this.msgError = '';
    this.loginService.authenticate({data: this.form.value}).subscribe(res => {
      if (res.success) {
        this.loginService.setToken(res.data.token);
        this.gotoHome();
      } else {
        this.msgError = res.message;
      }
    });
  }

  private gotoHome() {
    this.router.navigate(['/']);
  }

}
