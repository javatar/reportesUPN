import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IResponse} from '../../tools/interfaces/response';
import {HttpClient} from '@angular/common/http';
import {environment} from '@env/environment';

@Injectable()
export class LoginService {
  private urlLogin = 'login';

  constructor(private http: HttpClient) {
  }

  public authenticate(data: any): Observable<IResponse> {
    return this.http.post<IResponse>(environment.urlAPI + this.urlLogin, data);
  }

  public setToken(token: string): void {
    localStorage.setItem('_t', token);
  }
}
