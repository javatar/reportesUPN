import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'upn-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public items = [];

  constructor() {
  }

  ngOnInit() {
    this.items.push({
        title: 'Estados de cuenta',
        description: 'Módulo para visualizar el estado de cuenta del personal de la institución.',
        cssClass: 'Success',
        link: 'estado-cuenta'
      },
      {
        title: 'Departamentos',
        description: 'Módulo para visualizar el estado de cuenta del personal de la institución.',
        cssClass: 'Warning',
        link: 'departamentos'
      },
      {
        title: 'Viajes',
        description: 'Módulo para visualizar el estado de cuenta del personal de la institución.',
        cssClass: 'Info',
        link: 'viajes'
      });
  }

}
