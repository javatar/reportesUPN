import {Component, OnInit} from '@angular/core';
import {Parameter} from '../staff-statement/models/parameter';
import {Department} from './models/department';
import {DepartmentService} from './services/department.service';

@Component({
  selector: 'upn-departments',
  templateUrl: './departments.component.html'
})
export class DepartmentsComponent implements OnInit {
  public params: Parameter = new Parameter();
  public searching = false;
  public departments: Department[];

  constructor(private departmentService: DepartmentService) {
  }

  ngOnInit() {
  }

  search(params) {
    this.searching = true;

    this.params = {
      data: {
        entity: params.entity,
        month: null,
        year: params.year,
        id_persona: params.employee.id
      }
    };

    this.departmentService.find(this.params).subscribe(r => {
      if (r.data.items.length > 0) {
        this.departments = [];
        for (const d of r.data.items) {
          const department = new Department();

          department.id = d.id;
          department.nombre = d.name;
          department.saldo = d.saldo;
          department.saldoInicial = d.saldo_incial;
          department.ppto = d.ppto;
          department.totalPpto = d.total_ppto;
          department.realizado = d.realizado;
          department.ingreso = d.ingreso;

          this.departments.push(department);
        }
      } else {
        this.departments = null;
      }
      this.searching = false;
    });

  }


}
