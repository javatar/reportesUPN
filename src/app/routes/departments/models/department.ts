export class Department {
  id: string;
  nombre: string;
  ppto: number;
  saldoInicial: number;
  totalPpto: number;
  realizado: number;
  ingreso?: number;
  saldo: number;
  porcentajeRealizado ?: number;
  childOpen = false;
}
