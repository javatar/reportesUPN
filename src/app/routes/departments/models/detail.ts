export class Detail {
  total: number;
  items?: DetailItem[];
}

export class DetailItem {
  comentario: string;
  valor: number;
  fecha: string;
}
