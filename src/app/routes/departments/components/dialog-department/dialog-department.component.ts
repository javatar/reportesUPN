import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DepartmentService} from '../../services/department.service';
import {Detail, DetailItem} from '../../models/detail';

@Component({
  selector: 'upn-dialog-department',
  templateUrl: './dialog-department.component.html'
})
export class DialogDepartmentComponent implements OnInit {
  public loadingDetails = false;
  public detail: Detail;

  constructor(public dialogRef: MatDialogRef<DialogDepartmentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private departmentService: DepartmentService) {
  }

  ngOnInit() {
    this.loadDetails();
  }

  loadDetails() {
    this.loadingDetails = true;
    this.data.params.data.id_depto = this.data.department.id;
    this.departmentService.findDetails(this.data.params).subscribe(r => {
      if (r.data.items.length > 0) {
        this.detail = new Detail();
        this.detail.total = r.data.total.total;
        this.detail.items = [];

        for (const i of r.data.items) {
          const item = new DetailItem();
          item.comentario = i.comentario;
          item.fecha = i.fecha;
          item.valor = i.valor;

          this.detail.items.push(item);
        }
      } else {
        this.detail = null;
      }
    }, null, () => {
      this.loadingDetails = false;
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

}
