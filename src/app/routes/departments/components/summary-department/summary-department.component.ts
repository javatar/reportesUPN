import {Component, Input, OnInit} from '@angular/core';
import {Department} from '../../models/department';

@Component({
  selector: 'upn-summary-department',
  templateUrl: './summary-department.component.html'
})
export class SummaryDepartmentComponent implements OnInit {
  @Input() data: Department;

  public chartLabels: string[] = ['Realizado', 'Saldo'];
  public chartData: number[] = [];
  public chartColors: any[] = [{backgroundColor: ['43a047', 'fb8c00']}];

  constructor() {
  }

  ngOnInit() {
    this.chartData = [this.data.realizado, this.data.saldo];
  }

}
