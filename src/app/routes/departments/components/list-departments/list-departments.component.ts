import {Component, Input, OnInit} from '@angular/core';
import {Department} from '../../models/department';
import {DialogDepartmentComponent} from '../dialog-department/dialog-department.component';
import {MatDialog} from '@angular/material';
import {Parameter} from '../../../staff-statement/models/parameter';

@Component({
  selector: 'upn-list-departments',
  templateUrl: './list-departments.component.html'
})
export class ListDepartmentsComponent implements OnInit {
  @Input() data: Department[];
  @Input() params: Parameter;
  @Input() isLoading = false;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(data): void {
    const _data = {
      department: data,
      params: this.params
    };
    this.dialog.open(DialogDepartmentComponent, {
      data: _data,
      width: '100%'
    });
  }

}
