import {Component, Input, OnInit} from '@angular/core';
import {Detail} from '../../models/detail';

@Component({
  selector: 'upn-detail-department',
  templateUrl: './detail-department.component.html'
})
export class DetailDepartmentComponent implements OnInit {
  @Input() data: Detail;
  @Input() isLoading: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
