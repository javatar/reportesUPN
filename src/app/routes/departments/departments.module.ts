import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DepartmentsRoutingModule} from './departments-routing.module';
import {FilterModule} from '../../tools/components/filter/filter.module';
import {LoaderModule} from '../../tools/components/loader/loader.module';
import {DepartmentsComponent} from './departments.component';
import {EmployeeService, EntityService, MonthService, YearService} from '@providers/services';
import {MatDialogModule} from '@angular/material';
import {DialogDepartmentComponent} from './components/dialog-department/dialog-department.component';
import {ListDepartmentsComponent} from './components/list-departments/list-departments.component';
import {DepartmentService} from './services/department.service';
import {ChartsModule} from 'ng2-charts';
import {SummaryDepartmentComponent} from './components/summary-department/summary-department.component';
import {DetailDepartmentComponent} from './components/detail-department/detail-department.component';

@NgModule({
  imports: [
    CommonModule,
    DepartmentsRoutingModule,
    FilterModule,
    LoaderModule,
    MatDialogModule,
    ChartsModule
  ],
  declarations: [
    DepartmentsComponent,
    DialogDepartmentComponent,
    ListDepartmentsComponent,
    SummaryDepartmentComponent,
    DetailDepartmentComponent],
  providers: [
    EntityService,
    YearService,
    MonthService,
    EmployeeService,
    DepartmentService
  ],
  entryComponents: [
    DialogDepartmentComponent
  ]
})
export class DepartmentsModule {
}
