import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {LayoutModule} from './tools/components/layout/layout.module';
import {AuthService} from './tools/providers/services/auth.service';
import {AuthGuard} from './tools/providers/guards/auth.guard';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {InterceptorsModule} from './tools/global/interceptors/interceptors.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    InterceptorsModule
  ],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
