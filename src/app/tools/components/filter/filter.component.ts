import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Employee, GroupEntity, Month, Year} from './models';
import {EntityService} from '@providers/services/entity.service';
import {YearService} from '@providers/services/year.service';
import {MonthService} from '@providers/services/month.service';
import {EmployeeService} from '@providers/services/employee.service';

@Component({
  selector: 'upn-filter',
  templateUrl: './filter.component.html',
  styles: [],
})
export class FilterComponent implements OnInit {

  @Output() onSearch = new EventEmitter<any>();
  @Input() showMonths = true;

  public month = new FormControl('', Validators.required);
  public year = new FormControl('', Validators.required);
  public employee = new FormControl('', Validators.required);
  public entity = new FormControl('', Validators.required);

  public months: Month[];
  public years: Year[];
  public groupsEntity: GroupEntity[];
  public employees: Employee[];
  public imageEmployee: string;


  constructor(private entityService: EntityService,
              private yearService: YearService,
              private monthService: MonthService,
              private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.resolveData();
    this.subscribeChanges();
    this.imageEmployee = './assets/img/profile/male-profile.jpg';
  }

  private resolveData() {
    this.entityService.getEntitiesGroupByType$()
      .subscribe(function (r) {
        this.groupsEntity = r.data.items;
      }.bind(this));

    this.yearService.getYears$()
      .subscribe(function (r) {
        this.years = r.data.items;
      }.bind(this));

    if (this.showMonths) {
      this.monthService.getMeses$()
        .subscribe(function (r) {
          this.months = r.data.items;
        }.bind(this));
    }
  }

  private subscribeChanges() {
    this.entity.valueChanges.subscribe(entity => {
      this.resolveEmployees();
    });

    this.year.valueChanges.subscribe(year => {
      this.resolveEmployees();
    });
    if (this.showMonths) {
      this.month.valueChanges.subscribe(month => {
        this.resolveEmployees();
      });
    }

  }

  public selectEmployee(): void {
    const emp = this.employees.filter(e => e.id === this.employee.value)[0];
    this.imageEmployee = 'http://intranet.educacionadventista.org.pe/fotos_upn/' + emp.doc_number + '.png';
  }

  private resolveEmployees() {
    if ((this.entity.valid && this.year.valid && this.month.valid && this.showMonths) ||
      (this.entity.valid && this.year.valid && !this.showMonths)) {
      const params = {
        entity: this.entity.value,
        year: this.year.value,
        month: null,
      };

      if (this.showMonths) {
        params.month = this.month.value;
      }

      this.employeeService.getListEmployee({data: params})
        .subscribe(r => {
          this.employees = r.data.persons;
          this.employee.enable();
        });
    } else {
      this.employees = [];
      this.employee.setValue('');
      this.employee.disable();
    }
  }

  public search() {
    const params = {
        entity: this.entity.value,
        year: this.year.value,
        month: this.month.value,
        employee: this.employees.filter(e => e.id === this.employee.value)[0],
      }
    ;

    this.onSearch.emit(params);

  }
}
