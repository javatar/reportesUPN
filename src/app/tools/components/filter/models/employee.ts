export class Employee {
  public id: string;
  public name: string;
  public entity: string;
  public doc_number: string;
}
