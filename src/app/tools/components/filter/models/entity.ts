export class Entity {
  id: string;
  name: string;
}

export class GroupEntity {
  id: string;
  name: string;
  entidades: Entity[];
}
