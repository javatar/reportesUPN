import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'upn-layout',
  template: `
    <div class="wrapper">
      <div class="sidebar">
        <upn-sidebar></upn-sidebar>
        <div class="sidebar-background"></div>
        <router-outlet name="main"></router-outlet>
      </div>
      <div class="main-panel">
        <upn-navbar></upn-navbar>
        <div class="main-content">
          <div class="container-fluid">
            <router-outlet></router-outlet>
          </div>
        </div>
        <upn-footer></upn-footer>
      </div>
    </div>
  `,
  styles: [``]
})
export class LayoutComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
