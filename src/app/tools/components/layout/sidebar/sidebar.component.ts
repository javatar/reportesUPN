import {Component, OnInit} from '@angular/core';

declare const $: any;

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {path: '/dashboard', title: 'Inicio', icon: 'dashboard', class: ''},
  {path: '/estado-cuenta', title: 'Estado de Cuenta', icon: 'person', class: ''},
  {path: '/departamentos', title: 'Departamentos', icon: 'content_paste', class: ''},
  {path: '/viajes', title: 'Viajes', icon: 'library_books', class: ''},
];

@Component({
  selector: 'upn-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
