/**
 * @license
 * Copyright Lamb Team. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import { Observable } from 'rxjs';
import {IResponse} from './response';

export interface GenericHttp {
  /**
   * URL del recurso.
   * @type {string}
   */
  url: string;

  /**
   * Retorna un a lista de objetos según el recurso.
   * @param params: Es opcional para filtrar la lista.
   * @return Observable<IResponse>.
   */
  getList$?(params?: string): Observable<IResponse>;

  /**
   * Retorna un objecto segun su ID.
   * @param id ID del objeto a traer.
   * @return Observable<IResponse>.
   */
  getById$?(id: string): Observable<IResponse>;

  /**
   * Guarda un objecto.
   * @param data Objecto a guardar.
   * @return Observable<IResponse>.
   */
  save$?(data: any): Observable<IResponse>;


  /**
   * Actualiza un Objecto segun su id.
   * @param id Id del objecto a editar.
   * @param data Objeto a editar.
   * @return Observable<IResponse>.
   */
  update$?(id: string, data: any): Observable<IResponse>;

  /**
   * Actualiza un Objecto segun su id.
   * @param id Id del objecto a editar.
   * @param data Objeto a editar.
   * @return Observable<IResponse>.
   */
  // put$?(id: string, data: any): Observable<IResponse>;

  /**
   * Guarda un nuevo objecto.
   * @param data Objeto a Crear.
   * @return Observable<IResponse>.
   */
  // post$?(data: any): Observable<IResponse>;

  /**
   * Actualiza ciertos atributos de un objecto.
   * @param id ID de los objetos a editar.
   * @param data Objecto json con los atributos a editar.
   * @return Observable<IResponse>
   */
  patch$?(id: string, data: any): Observable<IResponse>;

  /**
   * Elimina un objecto segun su Id.
   * @param id ID del objecto a eliminar.
   * @return Observable<IResponse>.
   */
  delete$?(id: string): Observable<IResponse>;
}
