import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from '@providers/services/auth.service';

@Injectable()
export class TokenInterceptorService
  implements HttpInterceptor {
  private token = '';

  constructor(private authService: AuthService) {
  }

  private subscribeToTokenChanges() {
    this.setToken(this.authService.getToken());
  }

  private setToken = (token: string) => (this.token = token);

  public intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.subscribeToTokenChanges();
    const authorizedReq = this.setAuthHeader(req);
    const handledRequest = next.handle(authorizedReq);
    return handledRequest;
  }

  private setAuthHeader(
    req: HttpRequest<any>
  ): HttpRequest<any> {
    const authToken = `${this.token}`;
    const headers = req.headers.set('Authorization', authToken);
    const authorizedReq = req.clone({headers});
    return authorizedReq;
  }
}
