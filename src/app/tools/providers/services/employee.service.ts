import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class EmployeeService {
  private url = environment.urlAPI + 'mobile/persons_year';

  constructor(private http: HttpClient) {
  }

  getListEmployee(sendData: any): Observable<IResponse> {
    return this.http.post<IResponse>(this.url, sendData);
  }
}
