import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class MonthService {
  private url = environment.urlAPI + 'setup/getMonth';
  private urlm = 'setup/months-entities';

  constructor(private http: HttpClient) {
  }

  public getMeses$(): Observable<IResponse> {
    return this.http.get<IResponse>(this.url);
  }

  public getMonthByEntity$(senData: any): Observable<IResponse> {
    return this.http.get<IResponse>(this.urlm, {params: senData});
  }
}
