import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {GenericHttp} from '@interfaces/generic-http';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class YearService implements GenericHttp {
  public readonly url = environment.urlAPI + 'setup/getYear';

  constructor(private http: HttpClient) {
  }

  public getYears$(): Observable<IResponse> {
    return this.http.get<IResponse>(this.url);
  }

  public getYearsByEntityId$(entity: string): Observable<IResponse> {
    return this.http.get<IResponse>(`${this.url}/${entity}`);
  }

}
