import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {IResponse} from '@interfaces/response';
import {environment} from '@env/environment';

@Injectable()
export class EntityService {
  private url = environment.urlAPI + 'setup/get_entity_by_type';

  constructor(private http: HttpClient) {
  }

  public getEntitiesGroupByType$(): Observable<IResponse> {
    return this.http.get<IResponse>(this.url);
  }
}
