import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {
  constructor(private route: Router) {
  }

  sendToken(token: string) {
    localStorage.setItem('_t', token);
  }

  getToken() {
    return localStorage.getItem('_t');
  }

  isLoggednIn() {
    return this.getToken() !== null;
  }

  logout() {
    localStorage.removeItem('_t');
    this.route.navigate(['login']);
  }
}
