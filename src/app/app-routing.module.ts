import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './tools/components/layout/layout.component';
import {AuthGuard} from './tools/providers/guards/auth.guard';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: 'src/app/routes/home/home.module#HomeModule'
      },
      {
        path: 'dashboard',
        redirectTo: '/',
        pathMatch: 'full'
      },
      {
        path: 'estado-cuenta',
        loadChildren: 'src/app/routes/staff-statement/staff-statement.module#StaffStatementModule'
      },
      {
        path: 'viajes',
        loadChildren: 'src/app/routes/travels/travels.module#TravelsModule'
      },
      {
        path: 'departamentos',
        loadChildren: 'src/app/routes/departments/departments.module#DepartmentsModule'
      }
    ],
    canActivate: [AuthGuard]
  },

  {
    path: 'login',
    loadChildren: 'src/app/routes/login/login.module#LoginModule'
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
